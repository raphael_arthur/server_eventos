<?php
	/*
	 * This STATIC class, implements encryption and decryption of data.
	 * Also create a password cypher string
	 */
class Security{

	public function decrypt($data, $key){
		$data = base64_decode($data);
		$salt = substr($data, 0, 16);
		$ct = substr($data, 16);

		$rounds = 3; //Because the server maybe slow...
		$data00 = $key . $salt;
		$hash = array();
		$hash[0] = hash('sha256', $data00, true);

		$result = $hash[0];
		for($i = 1; $i < $rounds; $i++){
			$hash[$i] = hash('sha256', $hash[$i - 1] . $data00, true);
			$result .= $hash[$i];
		}

		$dKey = substr($result, 0, 32);
		$iv = substr($result, 32, 16);

		return openssl_decrypt($ct, 'AES-256-CBC', $dKey, true, $iv);
	}

	public function encrypt($data){

		$salt = openssl_random_pseudo_bytes(16);

		$saltedData = '';
		$dx = '';
		// Salt the key(32) and iv(16) = 48
		while (strlen($saltedData) < 48) {
			$dx = hash('sha256', true);
			$saltedData .= $dx;
		}

		$eKey = substr($saltedData, 0, 32);
		$iv  = substr($saltedData, 32,16);

		$encrypted_data = openssl_encrypt($data, 'AES-256-CBC', $eKey, true, $iv);
		return base64_encode($salt . $encrypted_data);
	}

	/*
	 *  generate a randon 32 bits data and encrypt it
	 *  @param void
	 *  @return (String) random encrypted string
	 */

	public function generateKey(){
		//$key = openssl_random_pseudo_bytes(32);
        //Can't use this in PHP versions below 5.4
		//$key = password_hash($key, PASSWORD_DEFAULT);
         return substr(str_shuffle(".ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz012346789@?#"), 0, 64);
	}

	/*
	 *  Validades the user token, this function will be called upon each client request
	 *  @param (String) $userToken
	 *  @return (String) a "true" or "false" string that the user was validadeted
	 *	if the session was not validadeted destroy the session and deny the connection_aborted
	 *
	 */

	public function authSession($userToken, $isSigned, $HTTP, $REMOTE){
        session_start();
		//session_regenerate_id(true);
        //print $userToken . '<br />';
        //print $_SESSION['userToken'];
		//print $_SESSION['isSigned'] . '<br />';
		//print $_SESSION['HTTP_USER_AGENT'] . '<br />';
		//print $_SESSION['REMOTE_ADDR'] . '<br />';
		//print_r($_SESSION);
		if(isset($isSigned) and isset($userToken) and isset($HTTP) and isset($REMOTE)){
			//print 'var setadas';
			//verify the identity of the user
			if($isSigned == 'true'){
				//print 'here';
                return true;
			}
		} else {
			//print 'deu treta';
			session_destroy();
			unset($_SESSION);
			return false;
		}
		session_write_close();
	}

    /*
	 *  generate a randon 32 bits data and encrypt it
	 *  @param void
	 *  @return (Array) [requestStatus:boolean, newToken:string]
	 */
     function validadeRequest($userToken, $isSigned, $HTTP, $REMOTE){
        session_start();

        if($this->authSession($userToken, $isSigned, $HTTP, $REMOTE)){
			//print 'entrei if';
            $_SESSION["userToken"] = $this->generateKey();
            return array('requestStatus' => true, 'userToken' => $_SESSION["userToken"]);
        } else {
			//print 'nao entrei if';
            return array('requestStatus' => false, 'userToken' => "");
        }
     }
	/*
	*	Get the user agent,
	*	Obs.: I can often change this later, that's why the method call aproach
	*	@param (void)
	*	@return (String) userAgent
	*
	*/
	public function getUserAgent(){
		return $_SERVER['HTTP_USER_AGENT'];
	}

	/*
	*	Get the user IP,
	*	Obs.: I can often change this later, that's why the method call aproach
	*	@param (void)
	*	@return (String) userAgent
	*
	*/
	public function getUserIP(){
		return $_SERVER['REMOTE_ADDR'];
	}

    /*------------------------------------- PHP 5.3 VERSION ONLY ----------------------------
     -------------------------------------- DO NOT USE THIS IN PRODUCTION -------------
     --------------------------------------------------------------------------------- */
    /*
	*	Verify two hashed strings
	*	Obs.: I can often change this later, that's why the method call aproach
	*	@param (void)
	*	@return (String) userAgent
	*
	*/
    public function passwordVerify($dbPassword, $passwordInput){
        if (CRYPT_SHA512 != 1) {
            print "Fatal Error! Server Does not Support SHA-512 encyption";
            return false;
        } else {
            if (crypt($passwordInput, $dbPassword) == $dbPassword){
				return true;
            } else {
                return false;
            }
        }
    }
    /*
	*	Encrypt a string using SHA-512
	*	Obs.: I can often change this later, that's why the method call aproach
	*	@param (string) string
	*	@return (String) hashed_password
	*
	*/
    // public function encOneWayString($string){
    //      $salt = substr(str_shuffle("./ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz012346789"), 0, 8);
    //     if (CRYPT_SHA512 != 1) {
    //         print "Fatal Error! Server Does not Support SHA-512 encyption";
    //         return false;
    //     } else {
    //         return crypt($string, '$6$'.$salt);
    //     }
    // }
}
?>
