<?php
require_once "Database.class.php";
require_once "Sanitase.class.php";
require_once "Security.class.php";

class User {
    private $password;
    private $id;
    private $Database;

    function __construct(){
        $this->Database = new Database();
    }

    private function incrementSigninAttempt() {
        $this->Database->query("UPDATE usuario SET tentativas = tentativas + 1 WHERE idusuario = 1");
    }

    private function resetSigninAttempt() {
        $this->Database->query("UPDATE usuario SET tentativas = 0 WHERE idusuario = 1");
    }

    //Will return the string "true" if login was a sucess, and restart the attempts of password, if the user get the password wrong three times we will block the login.
    public function signin($email, $password){
        $Sanitase = new Sanitase();
        $Security = new Security();

        $email = $Sanitase->clearInsertion($email);

        $this->id = $this->Database->selectSingleton("SELECT idusuario FROM usuario WHERE email = '$email'","idusuario");

        $signinAttempts = $this->Database->selectSingleton("SELECT tentativas FROM usuario WHERE idusuario = '$this->id'","tentativas");

        if($signinAttempts > 3){
            //print "Your Account has been Blocked due too many passwords tentative. Please Contact an Administrator of Your System";
            //parse the info to the user in an JSON odbc_fetch_object
            return json_encode(array('userBlocked'=>'true'));
        } else {
            $password = $Sanitase->clearInsertion($Sanitase->ValidadeString($password));

            $dbpassword = $this->Database->selectSingleton("SELECT senha FROM usuario WHERE idusuario = '$this->id'","senha");

            

            if($Security->passwordVerify($dbpassword, $password)){
                $this->resetSigninAttempt();
           		session_start();
           		session_regenerate_id(true);

                $_SESSION['isSigned'] = "true";
                // $_SESSION['userToken'] = $Sanitase->cleanInput($Sanitase->cleanInput($Security->generateKey()));
                $_SESSION['userToken'] = $Security->generateKey();
                $_SESSION['REMOTE_ADDR'] = $Security->getUserIP();
                $_SESSION['HTTP_USER_AGENT'] = $Security->getUserAgent();

                $_SESSION["userID"] = $this->id;
                $_SESSION["userEmail"] = $email;
                session_write_close();
                //print_r($_SESSION);
                //parse the info to the user in an JSON odbc_fetch_object
                return json_encode(array('userToken' => $_SESSION['userToken'], 
                        'isSigned' => $_SESSION['isSigned'], 
                        'REMOTE_ADDR' => $_SESSION['REMOTE_ADDR'],
                        'HTTP_USER_AGENT' => $_SESSION['HTTP_USER_AGENT'],
                        'userID' => $_SESSION["userID"],
                        'userEmail' => $_SESSION["userEmail"]));
            } else {
                $this->incrementSigninAttempt();
                return json_encode(array('userToken' => '', 
                        'isSigned' => '', 
                        'REMOTE_ADDR' => '',
                        'HTTP_USER_AGENT' => '',
                        'userID' => '',
                        'userEmail' => ''));
            }
        }
    }
}
?>
