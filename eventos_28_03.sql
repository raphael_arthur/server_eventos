-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: plataforma_eventos
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `atividade`
--

DROP TABLE IF EXISTS `atividade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `atividade` (
  `idatividade` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `evento_idevento` int(11) NOT NULL,
  `evento_usuario_idusuario` int(11) NOT NULL,
  PRIMARY KEY (`idatividade`),
  KEY `fk_atividade_evento1_idx` (`evento_idevento`,`evento_usuario_idusuario`),
  CONSTRAINT `fk_atividade_evento1` FOREIGN KEY (`evento_idevento`, `evento_usuario_idusuario`) REFERENCES `evento` (`idevento`, `usuario_idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `atividade`
--

LOCK TABLES `atividade` WRITE;
/*!40000 ALTER TABLE `atividade` DISABLE KEYS */;
/*!40000 ALTER TABLE `atividade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evento`
--

DROP TABLE IF EXISTS `evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evento` (
  `idevento` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `inicio` datetime DEFAULT NULL,
  `fim` datetime DEFAULT NULL,
  `endereco` varchar(45) DEFAULT NULL,
  `local` varchar(45) DEFAULT NULL,
  `descricao` longtext,
  `usuario_idusuario` int(11) NOT NULL,
  PRIMARY KEY (`idevento`,`usuario_idusuario`),
  KEY `fk_evento_usuario_idx` (`usuario_idusuario`),
  CONSTRAINT `fk_evento_usuario` FOREIGN KEY (`usuario_idusuario`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evento`
--

LOCK TABLES `evento` WRITE;
/*!40000 ALTER TABLE `evento` DISABLE KEYS */;
INSERT INTO `evento` VALUES (2,'Evento 2','2017-05-11 00:00:00','2017-05-13 00:00:00','CLN 404 BLOCO E','LARA','descriÃ§Ã£o aleatoria',2),(3,'Evento 3','2017-05-15 00:00:00','2017-05-17 00:00:00','FT','auditorio','alsdjflasjdfadf',2);
/*!40000 ALTER TABLE `evento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inscritos`
--

DROP TABLE IF EXISTS `inscritos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inscritos` (
  `idinscritos` int(11) NOT NULL AUTO_INCREMENT,
  `primeiro_nome` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `cpf` varchar(45) DEFAULT NULL,
  `matricula` varchar(45) DEFAULT NULL,
  `evento_idevento` int(11) NOT NULL,
  `evento_usuario_idusuario` int(11) DEFAULT NULL,
  `ultimo_nome` varchar(45) DEFAULT NULL,
  `nascimento` date DEFAULT NULL,
  `presenca` int(11) DEFAULT '0',
  PRIMARY KEY (`idinscritos`),
  KEY `fk_inscritos_evento1_idx` (`evento_idevento`,`evento_usuario_idusuario`),
  CONSTRAINT `fk_inscritos_evento1` FOREIGN KEY (`evento_idevento`, `evento_usuario_idusuario`) REFERENCES `evento` (`idevento`, `usuario_idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inscritos`
--

LOCK TABLES `inscritos` WRITE;
/*!40000 ALTER TABLE `inscritos` DISABLE KEYS */;
INSERT INTO `inscritos` VALUES (1,'Paulo','paulo.kimura@cdt.unb.br','12345678910','123456',2,NULL,'Kimura','2017-05-17',0);
/*!40000 ALTER TABLE `inscritos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inscritos_has_atividade`
--

DROP TABLE IF EXISTS `inscritos_has_atividade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inscritos_has_atividade` (
  `inscritos_idinscritos` int(11) NOT NULL,
  `atividade_idatividade` int(11) NOT NULL,
  PRIMARY KEY (`inscritos_idinscritos`,`atividade_idatividade`),
  KEY `fk_inscritos_has_atividade_atividade1_idx` (`atividade_idatividade`),
  KEY `fk_inscritos_has_atividade_inscritos1_idx` (`inscritos_idinscritos`),
  CONSTRAINT `fk_inscritos_has_atividade_atividade1` FOREIGN KEY (`atividade_idatividade`) REFERENCES `atividade` (`idatividade`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_inscritos_has_atividade_inscritos1` FOREIGN KEY (`inscritos_idinscritos`) REFERENCES `inscritos` (`idinscritos`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inscritos_has_atividade`
--

LOCK TABLES `inscritos_has_atividade` WRITE;
/*!40000 ALTER TABLE `inscritos_has_atividade` DISABLE KEYS */;
/*!40000 ALTER TABLE `inscritos_has_atividade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `senha` mediumtext NOT NULL,
  `email` varchar(45) NOT NULL,
  `tentativas` int(11) DEFAULT '0',
  `login` varchar(45) NOT NULL,
  PRIMARY KEY (`idusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (2,'arthur','$1$DZ61sXtS$vD7pDMFvFW/gth03Oc0RR0','raphael.arthur@hotmail.com',0,'raphael');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-28 15:36:43
