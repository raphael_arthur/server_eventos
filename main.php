<?php
    require_once('class/Database.class.php');
    require_once('class/Security.class.php');
    require_once('class/Session.class.php');
    
    /*
    Start a new User Session;
    */

    error_reporting(E_ALL);
    ini_set('display_errors', 1);

    $session = new Session();
    session_set_save_handler($session);
    session_start();
    
?>
